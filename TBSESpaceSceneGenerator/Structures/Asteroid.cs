﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    public enum AsteroidType
    {
        CType,
        SType,
        MType,
        Count
    }

    class Asteroid
    {
        public AsteroidType Type { get; set; }

        public double Diameter { get; set; }

        public double Mass { get; set; }

        public double Density { get; set; }

        public int Temperature { get; set; }
    }
}
